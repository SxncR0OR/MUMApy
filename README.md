# MUMApy


## Synopsis

**MUMApy**, alias **MUltilingual MAnager for Python**, est une bibliothèque en langage Python qui permet de gérer plusieurs langages directement dans le code de l'application.

**MUMApy** utilise la syntaxe de **gettext** pour les chaînes de caractère.



## Exemple d'utilisation

Toute chaîne de caractère présente dans le code prendra cette forme-ci: `_("exemple")`.  
Par exemple:
```Python
print(_('hello'))
```

Quelque part dans votre code, vous aurez fait la déclaration suivante:
```Python
muma.add(label="hello",translations=dict(en="Hello",fr="Bonjour",es="Hola"))
```

Notez que si votre traduction est plus longue, vous pouvez aussi la faire sur plusieurs lignes:
```Python
muma.add(label="hello-world",translations=dict(en="Hello World!"))
muma.add(label="hello-world",translations=dict(fr="Bonjour tout le monde !"))
muma.add(label="hello-world",translations=dict(es="¡ Hola mundo !"))
```

Il est important de comprendre que la chaîne de caractères 'hello-world' est une étiquette qui renvoie vers ses différentes traductions. Ce n'est pas une des traductions mais juste une phrase ou un mot mnémotechnique.

MUMApy utilise l'étiquette pour retrouver la traduction dans le dictionnaire que vous aurez constitué.  
Par convention, cette étiquette est souvent en anglais mais ce n'est pas obligatoire.

Comme vous le constatez, le paramètre `translations=` de la fonction prend comme argument un dictionnaire python `dict()`. Ceci est valable même si vous n'ajoutez qu'une seule traduction au début.



## Installation dans votre programme


### Importer MUMApy comme fichier externe ###

*Note: Ceci est la méthode conseillée.*

Placez le fichier nommé `MUMApy.py` au même endroit que votre programme.

Au début de votre code source, ajoutez:
```Python
from MUMApy import MUMAclass
muma=MUMAclass(lang='en')
def _(label,lang=None):
	return muma.getMessage(label,lang)
```

Selon vos besoins, changez `lang='en'` en `lang='fr'` par exemple.  
Vous pourrez changer de langue à n'importe quel moment.


### Placer MUMApy directement dans votre code ###

Une autre solution consiste à copier la classe nommée **MUMAclass** directement dans votre code source. Ainsi, vous n'ajouterez aucun autre fichier à votre distribution. Cette classe se situe dans le fichier `MUMApy.py`.

Après avoir copié/collé la classe au début de votre code source, ajoutez:
```Python
muma=MUMAclass(lang='en')
def _(label,lang=None):
	return muma.getMessage(label,lang)
```

Vous devez placer ce bout de code à un endroit accessible globalement pour toutes vos fonctions et bien sûr *après* la classe MUMAclass.  
En général, le début de votre programme est un bon endroit pour cela.  
Rappelez-vous que MUMAclass *ne pourra pas être accessible avant sa déclaration*.

Veuillez noter que vous devez continuer à respecter la licence de MUMApy lorsque vous l'intégrez à votre code. Votre licence doit notamment être compatible avec celle de MUMApy (voir la section Licence) et bien entendu vous ne pouvez pas enlever ou modifier le texte de la licence de MUMApy.



## Quelle est la différence avec gettext ?

**Les traductions se placent dans le code de l'application** et non dans des fichiers séparés.

Comme avec *gettext*, vous remplacez dans votre code chaque chaîne de caractère telle que `'exemple'` par `_('exemple')`.  
Le programme se chargera lui-même de la traduire dans le langage spécifié.

Pourquoi placer les traductions dans le code ?

Lorsque vous développez un petit logiciel, il peut arriver que vous vouliez limiter le nombre de fichiers afin de le garder aussi compact que possible. *Gettext* ne permet pas cela puisque de nombreux fichiers seront placés dans un répertoire réservé à son usage. À cause de cela, vous préférerez peut-être vous passer de support multi-langages en commençant. Mais c'est une mauvaise idée car cela va compliquer votre tâche par la suite...

MUMApy incite le programmeur à utiliser dès le départ la bonne syntaxe pour écrire les chaînes de caractère qui seront traduites.

> Prenons le cas de quelqu'un qui travaille seul sur un logiciel. Dans un premier temps, cette personne décide que son programme sera juste traduit en 2 langues: sa langue maternelle et l'anglais. En utilisant MUMApy le code source aura directement la bonne apparence, même s'il décide plus tard d'utiliser *gettext*. Alors qu'en optant pour *gettext*, le programmeur aurait probablement reporté le formatage de ses chaînes de caractère à la fin de son projet. À cause de cela, son programme serait probablement resté plus longtemps sans aucune traduction.



## Avantages

1. MUMApy est une classe très légère qui est conçue pour les petits projets qui ne veulent pas s'encombrer de nombreux fichiers de traduction.
0. MUMApy permet d'avoir toutes les traductions sous les yeux directement dans le code.
0. MUMApy s'utilise exactement comme *gettext*: la syntaxe des chaînes de caractère est la même.  
Si par la suite votre programme grandit, tout sera déjà prêt pour utiliser *gettext*.  
MUMApy permet même d'exporter les traductions déjà faites dans des fichiers PO/POT utilisables avec *gettext* !



## Contact

<mumapy.oliviervandecasteele@xoxy.net>


## Licence

MUMApy est distribué sous licence:  
[GNU Affero General Public License version 3 (AGPL v3)](https://www.gnu.org/licenses/agpl-3.0.en.html)

---

MUMApy is a python library allowing to manage several languages directly in your code  
Copyright (C) 2016 Olivier D.V. Vandecasteele  

This program is free software: you can redistribute it and/or modify  
it under the terms of the GNU Affero General Public License as  
published by the Free Software Foundation, either version 3 of the  
License, or (at your option) any later version.  

This program is distributed in the hope that it will be useful,  
but WITHOUT ANY WARRANTY; without even the implied warranty of  
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
GNU Affero General Public License for more details.  

You should have received a copy of the GNU Affero General Public License  
along with this program.  If not, see <http://www.gnu.org/licenses/>.
